<?php
include "php/insertDB.php";
?>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="otakoyi">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="icon" href="https://www.apple.com/favicon.ico" type="image/x-icon">
    <title>Visitors book</title>
    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">
    <!-- Web Application Manifest -->
    <!--	<link rel="manifest" href="manifest.json">-->
    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Web Starter Kit">
    <link rel="icon" sizes="192x192" href="">
    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Web Starter Kit">
    <link rel="apple-touch-icon" href="">
    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#2F3BA2">
    <!-- Color the status bar on mobile devices -->
    <meta name="theme-color" content="#2F3BA2">
    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
	<link rel="canonical" href="http://www.example.com/">
	-->
    <!-- Material Design icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.indigo-pink.min.css">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <!--    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>-->
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <!-- Your styles -->
    <link rel="stylesheet" href="css/mystyle.css">
</head>
<body>
<div class="container">
    <div class="row">
        <p>
            <a href="php/login.php">
                <button class="btn btn-outlined  btn-success">Зайти в кабінет модератора
                </button>
            </a></p>
        <div class="col-sm-4 col-sm-offset-4">
            <section class="animated bounce">
                <form action="<?php echo htmlentities ( $_SERVER['PHP_SELF'] ) ?>" method="POST">
                    <fieldset>
                        <!-- Form Name -->
                        <legend class="animated fadeInDown">Guest Book</legend>
                        <div class="form-group">
                            <label class=" control-label">User Name</label>
                            <div class=" inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input type="text" name="user" class="form-control" id="user" autofocus
                                           placeholder="Ваше Імя" value="<?php if ( isset( $user ) ) {
										echo $user;
									} ?>">
									<?php if ( isset( $err_user ) ) {
										echo $err_user;
									} ?>
									<?php if ( isset( $err_patternmatch ) ) {
										echo $err_patternmatch;
									} ?>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class=" control-label">E-mail</label>
                                <div class="inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-envelope"></i></span>
                                        <input type="text" name="email" class="form-control" id="email" autofocus
                                               placeholder="Ваш Email" value="<?php if ( isset( $email ) ) {
											echo $email;
										} ?>"/>
										<?php if ( isset( $err_email ) ) {
											echo $err_email;
										} ?>
										<?php if ( isset( $err_emaill ) ) {
											echo $err_emaill;
										} ?>
                                    </div>
                                </div>
                            </div>
                            <!-- Text area -->
                            <div class="form-group">
                                <label class=" control-label">Ваше повідомлення</label>
                                <div class=" inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-pencil"></i></span>
                                        <input type="text" name="comment" class="form-control"
                                               placeholder="Заборонені Html tags"
                                               value="<?php if ( isset( $comment ) ) {
											       echo $comment;
										       } ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" control-label">Ви не бот?</label>
                                <div class=" inputGroupContainer">
                                    <!--                        <div class="input-group">-->
                                    <!--                            <div class="g-recaptcha" data-sitekey="6LdUxS0UAAAAADgqvGH-h_hEGBfr1V1T1O-ez7cL"></div>-->
                                    <!--	                        --><?php //if ( isset( $err_captcha ) ) {
									//		                        echo $err_captcha;
									//	                        } ?>
                                </div>
                            </div>
                        </div>
                        <!-- Button -->
                        <div class="form-group">
                            <label></label>
                            <div>
                                <button type="submit" name="action" class="btn btn-info btn-lg btn-block"
                                        value="submit">
                                    Відправити дані <span class="glyphicon glyphicon-send"></span></button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <div>
                    <button type="button" id='italic'>Italic style</button>
                    <button type="button" id='bold'>Bold style</button>
                </div>
                <script>
                    $(document).ready(function () {
                        $('#italic').on('click', function () {
                            $('#style').toggleClass('italic');
                        });
                        $('#bold').on('click', function () {
                            $('#style').toggleClass('bold');
                        });
                    });
                </script>
                <button id="showResBtn" value="" class="btn btn-info btn-lg btn-block">Показати записи</button>
                <div id="showDbResults"></div>
            </section>
        </div>
    </div>
</div>
<script src="js/script.js"></script>
</body>
</html>
