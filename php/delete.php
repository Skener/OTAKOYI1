<?php
require 'config.php';
include "DBparam.php";
if ( empty( $_SESSION['name'] ) ) {
	header ( 'Location: login.php' );
}
if ( ( $_SERVER['REQUEST_METHOD'] === 'POST' ) ):
if ( isset( $_POST['id'] ) ) {
	$id = filter_var ( $_POST['id'], FILTER_SANITIZE_STRING );
	echo $id;
	try {
		include "DBparam.php";
		$pdo = new PDO( $dsn, $username, $password, $options );
	} catch ( PDOException $e ) {
		die( "Неможливо зєднатись " . $e->getMessage () );
	}

	try {
		$sql          = "DELETE FROM guestdata WHERE id = $id";
		$affectedrows = $pdo->exec ( $sql );
		if ( isset( $affectedrows ) ) {
			echo '<div class="alert alert-success text-center">Дані успішно видалені!</div>';
		}
	} catch ( PDOException $e ) {
		die( "Запит неможливо виконати $sql. " . $e->getMessage () );
	}

}
$stmt = $pdo->prepare ( "SELECT * FROM guestdata ORDER BY date DESC " );
$stmt->execute ();
$row_count = $stmt->rowCount ();
?>
<table class='table table-bordered table-hover table-condensed text-center' id='myTable'>
    <thead>
    <tr>
        <th class="text-center">Користувач</th>
        <th class="text-center">Email</th>
        <th class="text-center">Повідомлення</th>
        <th class="text-center">Ip</th>
        <th class="text-center">Browser</th>
        <th class="text-center">Створений</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th class="text-center">Користувач</th>
        <th class="text-center">Email</th>
        <th class="text-center">Повідомлення</th>
        <th class="text-center">Ip</th>
        <th class="text-center">Browser</th>
        <th class="text-center">Створений</th>
    </tr>
    </tfoot>
    <tbody>
	<?php
	if ( $row_count > 0 ) {
		while ( $row = $stmt->fetch () ) {
			//print_r ($row);
			echo '<tr>
<td>' . $row["user"] . '</td>
<td>' . $row['email'] . '</td>
<td>' . $row["comment"] . '</td>
<td>' . $row["ip"] . '</td>
<td>' . $row["browser"] . '</td>
<td>' . $row["date"] . '</td>
</tr>';
		}
		echo "</tbody></table>
Всього:<strong>{$row_count}</strong> результат(ів)";
	}
	unset( $pdo );
	endif
	?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#myTable').DataTable();
        });
    </script>
