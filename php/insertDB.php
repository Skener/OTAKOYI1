<?php
if ( ( $_SERVER['REQUEST_METHOD'] === 'POST' ) && ( ! empty( $_POST['action'] ) ) ):
	if ( isset( $_POST['user'] ) ) {
		$user = filter_var ( $_POST['user'], FILTER_SANITIZE_STRING );
	}
	if ( isset( $_POST['email'] ) ) {
		$email = filter_var ( $_POST['email'], FILTER_SANITIZE_STRING );
	}
	if ( isset( $_POST['comment'] ) ) {
		$comment = filter_var ( $_POST['comment'], FILTER_SANITIZE_STRING );
	}

	$formerrors = false;
	if ( $user === '' ) {
		$err_user   = '<div class="alert alert-danger" role="alert">Вибачте, це поле обовязкове</div>';
		$formerrors = true;
	}; // input field empty

	if ( $email === '' ) {
		$err_email  = '<div class="alert alert-danger" role="alert">Вибачте, це поле обовязкове</div>';
		$formerrors = true;
	}; // input field empty

	if ( ! filter_var ( $email, FILTER_VALIDATE_EMAIL ) ) {
		$err_emaill = '<div class="alert alert-danger" role="alert">Вибачте, не вірний формат скриньки</div>';
		$formerrors = true;
	};
// pattern doesn't match
	if ( ( preg_match ( '/[^\D$]/', $user ) ) ) {
		$err_patternmatch = '<div class="alert alert-danger" role="alert">Вибачте, лише  букви!</div>';
		$formerrors       = true;
	};
//	if ( isset( $_POST['g-recaptcha-response'] ) ) {
//		$captcha = $_POST['g-recaptcha-response'];
//	}
//        if(!$captcha){
//					$err_captcha = '<div class="alert alert-danger" role="alert">Перевірте капчу!</div>';
//					$formerrors = true;
//					 echo '<div class="alert alert-danger" role="alert">Перевірте капчу!</div>';
//        }
//	$response = file_get_contents ( "https://www.google.com/recaptcha/api/siteverify?secret=6LdUxS0UAAAAAAFkE3ua0qpHNjgb1_7h_oy97qkw&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR'] );

	$ip      = $_SERVER['REMOTE_ADDR'];
	$browser = $_SERVER['HTTP_USER_AGENT'];
	if ( ! ( $formerrors ) ):
		try {
			include "DBparam.php";
			$pdo = new PDO( $dsn, $username, $password, $options );
		} catch ( PDOException $e ) {
			die( "Неможливо зєднатись " . $e->getMessage () );
		}
		try {
// create prepared statement
			$sql  = 'INSERT INTO guestdata (user, email, comment, ip, browser) VALUES ( :user, :email, :comment, :ip, :browser)';
			$stmt = $pdo->prepare ( $sql );
			$stmt->bindParam ( ':user', $user, PDO::PARAM_STR );
			$stmt->bindParam ( ':email', $email, PDO::PARAM_STR );
			$stmt->bindParam ( ':comment', $comment, PDO::PARAM_STR );
			$stmt->bindParam ( ':ip', $ip, PDO::PARAM_STR );
			$stmt->bindParam ( ':browser', $browser, PDO::PARAM_STR );
			$stmt->execute ();
			if ( $stmt ) {
				echo '<div class="alert alert-success text-center">
                 Добавлено у базу:' . '<br>
                 <strong>' . $user . '<br>' . $email . '</strong></div>';
			}
		} catch ( PDOException $e ) {
			die( "Запит неможливо виконати $sql. " . $e->getMessage () );
		}
		unset( $pdo );
	endif;
endif;
?>
