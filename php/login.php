<?php
require 'config.php';

if ( isset( $_POST['login'] ) ) {
	$errMsg = '';

	// Get data from FORM
	$username = $_POST['username'];
	$password = $_POST['password'];

	if ( $username == '' ) {
		$errMsg = 'Введіть імя користувача';
	}
	if ( $password == '' ) {
		$errMsg = 'Введіть пароль';
	}

	if ( $errMsg == '' ) {
		try {
			$stmt = $connect->prepare ( 'SELECT id, fullname, username, password, secretpin FROM pdo WHERE username = :username' );
			$stmt->execute ( array (
				':username' => $username
			) );
			$data = $stmt->fetch ( PDO::FETCH_ASSOC );

			if ( $data == false ) {
				$errMsg = "Користувача $username не знайдено.";
			} else {
				if ( $password == $data['password'] ) {
					$_SESSION['name']      = $data['fullname'];
					$_SESSION['username']  = $data['username'];
					$_SESSION['password']  = $data['password'];
					$_SESSION['secretpin'] = $data['secretpin'];

					header ( 'Location: dashboard.php' );
					exit;
				} else {
					$errMsg = 'Невірний пароль';
				}
			}
		} catch ( PDOException $e ) {
			$errMsg = $e->getMessage ();
		}
	}
}
?>
<html>
<head>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <title>Login</title>
</head>
<style>
    html, body {
        margin: 1px;
        border: 0;
    }
</style>
<body>
<div align="center">
    <div style=" border: solid 1px #006D9C; " align="left">
		<?php
		if ( isset( $errMsg ) ) {
			echo '<div style="color:#FF0000;text-align:center;font-size:17px;">' . $errMsg . '</div>';
		}
		?>
        <div style="background-color:#006D9C; color:#FFFFFF; padding:10px;"><b>Login</b>
            <p>
                <a href="/index.php">
                    <button class="btn btn-outlined  btn-success">Назад
                    </button>
                </a>
            </p>
        </div>
        <div style="margin: 15px">
            <form action="" method="post">
                <input type="text" name="username" value="<?php if ( isset( $_POST['username'] ) )
					echo $_POST['username'] ?>" autocomplete="off" class="box"/><br/><br/>
                <input type="password" name="password" value="<?php if ( isset( $_POST['password'] ) )
					echo $_POST['password'] ?>" autocomplete="off" class="box"/><br/><br/>
                <input type="submit" name='login' value="Login" class='submit'/><br/>
            </form>
        </div>
    </div>
</div>
</body>
</html>
