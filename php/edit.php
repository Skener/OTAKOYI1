<?php
require 'config.php';
include "DBparam.php";
if ( empty( $_SESSION['name'] ) ) {
	header ( 'Location: login.php' );
}
?>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="otakoyi">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<link rel="icon" href="https://www.apple.com/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<title>Dashboard</title>
</head>
<style>
    html, body {
        margin: 1px;
        border: 0;
    }
</style>
<body>
<div align="center">
    <div style=" border: solid 1px #006D9C; " align="left">
		<?php
		if ( isset( $errMsg ) ) {
			echo '<div style="color:#FF0000;text-align:center;font-size:17px;">' . $errMsg . '</div>';
		}
		?>
        <div style="background-color:#006D9C; color:#FFFFFF; padding:10px;"><b><?php echo $_SESSION['name']; ?></b>
        </div>
        <div style="margin: 15px">
            Привіт <?php echo $_SESSION['name']; ?> <br>
            <p>
                <a href="/index.php">
                    <button class="btn btn-outlined  btn-success">Назад
                    </button>
                </a>
            </p>
            <a href="logout.php">Вийти</a>
			<?php
			try {
				$id = $_GET['id'];
				//echo $id;
				$dbs  = new PDO( $dsn, $username, $password, $options );
				$stmt = $dbs->prepare ( "SELECT * FROM guestdata WHERE id = $id" );
				$stmt->execute ();
				//$row_count = $stmt->rowCount ();
				$row       = $stmt->fetch ();
				$user      = $row["user"];
				$email     = $row["email"];
				$comment   = $row["comment"];
			} catch ( Exception $e ) {
				$e->getMessage ();
			}
			?>
            <div class="col-sm-4 col-sm-offset-4">
                <section class="animated bounce">
                    <form action="update.php?id=<?php echo $row['id']?> " method="POST">
                        <fieldset>
                            <!-- Form Name -->
                            <legend class="animated fadeInDown">Guest Book</legend>
                            <div class="form-group">
                                <label class=" control-label">User Name</label>
                                <div class=" inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input type="text" name="user" class="form-control" id="user" autofocus
                                               placeholder="Ваше Імя" value="<?php if ( isset( $user ) ) {
											echo $user;
										} ?>">
										<?php if ( isset( $err_user ) ) {
											echo $err_user;
										} ?>
										<?php if ( isset( $err_patternmatch ) ) {
											echo $err_patternmatch;
										} ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class=" control-label">E-mail</label>
                                    <div class="inputGroupContainer">
                                        <div class="input-group">
                                        <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-envelope"></i></span>
                                            <input type="text" name="email" class="form-control" id="email" autofocus
                                                   placeholder="Ваш Email" value="<?php if ( isset( $email ) ) {
												echo $email;
											} ?>"/>
											<?php if ( isset( $err_email ) ) {
												echo $err_email;
											} ?>
											<?php if ( isset( $err_emaill ) ) {
												echo $err_emaill;
											} ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- Text area -->
                                <div class="form-group">
                                    <label class=" control-label">Ваше повідомлення</label>
                                    <div class=" inputGroupContainer">
                                        <div class="input-group">
                                        <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-pencil"></i></span>
                                            <input type="text" name="comment" class="form-control"
                                                   placeholder="Заборонені Html tags"
                                                   value="<?php if ( isset( $comment ) ) {
												       echo $comment;
											       } ?>"
                                        </div>
                                    </div>
                                </div>
                                <!-- Button -->
                                <div class="form-group">
                                    <label></label>
                                    <div>
                                        <button type="submit" name="action" class="btn btn-info btn-lg btn-block"
                                                value="submit">
                                            Відправити дані <span class="glyphicon glyphicon-send"></span></button>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        $('#myTable').DataTable();
                                    });
                                </script>
                            </div>
            </div>
        </div>
</body>
</html>