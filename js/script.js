$(document).ready(function () {
    var commentCount = 5;
    //event.preventDefault();
    $('#showResBtn').on('click', function () {
        commentCount = commentCount + 5;
        $("#showDbResults").load("php/showDB.php", {
            commentNewCount: commentCount
        });
    });
    $('td #delBtn').on('click', function () {
        //event.preventDefault();
        var id = $(this).data('value');
        $.ajax({
            type: 'POST',
            url: 'delete.php',
            data: {id:id},
            cache: false,
            success: function (data) {
                $('#myTable').html(data);
            }
        });
    });

    $('#reset').on('click', function () {
        location.reload(true);
    })
});

