<?php
require 'config.php';
include "DBparam.php";
if ( empty( $_SESSION['name'] ) ) {
	header ( 'Location: login.php' );
}
if ( ( $_SERVER['REQUEST_METHOD'] === 'POST' ) && ( ! empty( $_POST['action'] ) ) ):
if ( isset( $_POST['user'] ) ) {
	$user = filter_var ( $_POST['user'], FILTER_SANITIZE_STRING );
}
if ( isset( $_POST['email'] ) ) {
	$email = filter_var ( $_POST['email'], FILTER_SANITIZE_STRING );
}
if ( isset( $_POST['comment'] ) ) {
	$comment = filter_var ( $_POST['comment'], FILTER_SANITIZE_STRING );
}

$formerrors = false;
if ( $user === '' ) {
	$err_user   = '<div class="alert alert-danger" role="alert">Вибачте, це поле обовязкове</div>';
	$formerrors = true;
}; // input field empty

if ( $email === '' ) {
	$err_email  = '<div class="alert alert-danger" role="alert">Вибачте, це поле обовязкове</div>';
	$formerrors = true;
}; // input field empty

if ( ! filter_var ( $email, FILTER_VALIDATE_EMAIL ) ) {
	$err_emaill = '<div class="alert alert-danger" role="alert">Вибачте, не вірний формат скриньки</div>';
	$formerrors = true;
};
// pattern doesn't match
if ( ( preg_match ( '/[^\D$]/', $user ) ) ) {
	$err_patternmatch = '<div class="alert alert-danger" role="alert">Вибачте, лише  букви!</div>';
	$formerrors       = true;
};
$id      = $_GET['id'];
$ip      = $_SERVER['REMOTE_ADDR'];
$browser = $_SERVER['HTTP_USER_AGENT'];
if ( ! ( $formerrors ) ):
try {
	include "DBparam.php";
	$pdo = new PDO( $dsn, $username, $password, $options );
} catch ( PDOException $e ) {
	die( "Неможливо зєднатись " . $e->getMessage () );
}
try {
// create prepared statement
$sql  = "UPDATE guestdata SET user = :user, 
 										  email = :email, 
 										  comment = :comment, 
 										  ip = :ip, 
 										  browser = :browser WHERE id = $id";
$stmt = $pdo->prepare ( $sql );
$stmt->bindParam ( ':user', $user, PDO::PARAM_STR );
$stmt->bindParam ( ':email', $email, PDO::PARAM_STR );
$stmt->bindParam ( ':comment', $comment, PDO::PARAM_STR );
$stmt->bindParam ( ':ip', $ip, PDO::PARAM_STR );
$stmt->bindParam ( ':browser', $browser, PDO::PARAM_STR );
$stmt->execute ();
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="otakoyi">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="icon" href="https://www.apple.com/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <title>Dashboard</title>
</head>
<style>
    html, body {
        margin: 1px;
        border: 0;
    }
</style>
<body>
<div align="center">
    <div style=" border: solid 1px #006D9C; " align="left">
		<?php
		if ( isset( $errMsg ) ) {
			echo '<div style="color:#FF0000;text-align:center;font-size:17px;">' . $errMsg . '</div>';
		}
		?>
        <div style="background-color:#006D9C; color:#FFFFFF; padding:10px;"><b><?php echo $_SESSION['name']; ?></b>
        </div>
        <div style="margin: 15px">
            Привіт <?php echo $_SESSION['name']; ?> <br>
            <p>
                <a href="/index.php">
                    <button class="btn btn-outlined  btn-success">Назад
                    </button>
                </a>
            </p>
            <a href="logout.php">Вийти</a>

			<?php
			if ( $stmt ) {
				echo '<div class="alert alert-success text-center">
                 Оновлено у базі на:' . '<br>
                 <strong>' . $user . '<br>' . $email . '</strong></div>';
			}
			} catch ( PDOException $e ) {
				die( "Запит неможливо виконати $sql. " . $e->getMessage () );
			}
			unset( $pdo );
			endif;
			endif;
			?>


