<?php
require 'config.php';
include "DBparam.php";
if ( empty( $_SESSION['name'] ) ) {
	header ( 'Location: login.php' );
}
?>
<html>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="otakoyi">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<link rel="icon" href="https://www.apple.com/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/script.js"></script>
<head><title>Dashboard</title></head>
<style>
    html, body {
        margin: 1px;
        border: 0;
    }
</style>
<body>
<div align="center">
    <div style=" border: solid 1px #006D9C; " align="left">
		<?php
		if ( isset( $errMsg ) ) {
			echo '<div style="color:#FF0000;text-align:center;font-size:17px;">' . $errMsg . '</div>';
		}
		?>
        <div style="background-color:#006D9C; color:#FFFFFF; padding:10px;"><b><?php echo $_SESSION['name']; ?></b>
        </div>
        <div style="margin: 15px">
            Привіт <?php echo $_SESSION['name']; ?> <br>
            <a href="logout.php">Вийти</a>
			<?php
			try {
			$dbs  = new PDO( $dsn, $username, $password, $options );
			$stmt = $dbs->prepare ( "SELECT * FROM guestdata ORDER BY date DESC" );
			$stmt->execute ();
			$row_count = $stmt->rowCount ();
			?>
            <table class='table table-bordered table-hover table-condensed text-center' id='myTable'>
                <thead>
                <tr>
                    <th class="text-center">Користувач</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Повідомлення</th>
                    <th class="text-center">Ip</th>
                    <th class="text-center">Browser</th>
                    <th class="text-center">Редагувати</th>
                    <th class="text-center">Видалити</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class="text-center">Користувач</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Повідомлення</th>
                    <th class="text-center">Ip</th>
                    <th class="text-center">Browser</th>
                    <th class="text-center">Редагувати</th>
                    <th class="text-center">Видалити</th>
                </tr>
                </tfoot>
                <tbody id='style'><?php
				if ( $row_count > 0 ) {
					while ( $row = $stmt->fetch () ) {
						//print_r ($row);
						echo '<tr>
<td>' . $row["user"] . '</td>
<td>' . $row['email'] . '</td>
<td>' . $row["comment"] . '</td>
<td>' . $row["ip"] . '</td>
<td>' . $row["browser"] . '</td>
<td><a href="edit.php?id=' . $row["id"] . '" class="btn btn-info"><span class="glyphicon glyphicon-edit"></span></a></td>
<td > <a data-value="' . $row["id"] . '" class="btn btn-xs btn-danger"  id="delBtn"><span class="glyphicon glyphicon-remove"></span></a></td>
</tr>';
					}
					echo "</tbody></table>
Всього:<strong>{$row_count}</strong> результат(ів)";
				}
				}
				catch ( PDOException $e ) {
					$e->getMessage () . "<br/>";
				}
				?>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#myTable').DataTable();
                    });
                </script>
        </div>
    </div>
</div>
</body>
</html>
